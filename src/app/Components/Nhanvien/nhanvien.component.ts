import {Component} from "@angular/core";
@Component({
    selector:"app-nhanvien",
    templateUrl: "./nhanvien.component.html",
    styles:[
        `.aoxanh{color:green;background-color:yellow; font-weight:bold;}`
    ]
})

export class NhanvienComponent{
    hoten:string = "Tran Van Teo";
    namsinh:number = 1987;
    mausac:string ="green";
    macao:boolean = true;
    hinh:string = "http://media.doisongphapluat.com/476/2016/10/27/hinh-anh-halloween-de-thuong1.jpg";

    constructor(){
        this.hoten = "Le Thanh Binh";
    }

    clickHinh(e){
        console.log(e.srcElement.src);
    }
}
