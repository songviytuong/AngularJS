import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HocsinhComponent } from './Components/Hocsinh/hocsinh.component';
import { NhanvienComponent } from './Components/Nhanvien/nhanvien.component';

@NgModule({
  declarations: [
    AppComponent,
    HocsinhComponent,
    NhanvienComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
